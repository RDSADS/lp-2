<?php

class At02 extends MY_controller{

        public function index(){
            $html = $this->load->view('pages/autor', null, true);
            $this->show($html);
        }

        public function rafael(){ 
            
            $html = $this->load->view('component/cardpesquisa', null, true);
            $html .= $this->show($html);   
        }

        public function playlist(){
            $this->load->model('pesquisaModel');
            $html = $this->load->view('pages/playlist', null, true);
            $html .= $this->show(NULL);   
        }

        public function relatorio(){
            $this->show(NULL);    
        }

        public function api(){ 
            $html = $this->load->view('pages/api', null, true);
            $html .= $this->load->view('component/cardapi', null, true);
            $this->show($html);
        }

}

?>