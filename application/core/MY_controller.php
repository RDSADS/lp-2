<?php

class MY_Controller extends CI_Controller{

    protected function show($content){
        $html = $this->load->view('common/header', null, true);
        $html .= $this->load->view('common/navbar', null, true);
        $html .= $content;
        $html .= $this->load->view('common/footer', null, true);
        echo $html;
    }
}