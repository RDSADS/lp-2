<div class="container">
    <div class="row">
        <div class="col-md-4 mt-4 mb-2">
            <div class="card text-center">
                <div class="view overlay">
                    <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
                    <a href="#!">
                    <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <div class="card-body">
                    <h4 class="card-title">Faça sua pesquisa:</h4>
                    <p class="card-text">
                        <form action="<?= base_url('at02/playlist') ?>" method="GET">
                            <div>
                                <input class="form-control mb-4" type="search" id="q" name="q" placeholder="Pesquisar">
                            </div>
                            <div>
                                Máximos resultados: <input class="form-control mb-4" type="number" id="maxResults" name="maxResults" min="1" max="50" step="1" value="5">
                            </div>
                            <input class="btn btn-primary" type="submit" value="Pesquisar">
                        </form>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>