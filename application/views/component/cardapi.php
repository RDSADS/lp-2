<div class="container mt-3">
    <div class="row">
        <div class="col-md-4 mt-4 mb-2">
            <div class="card">
                <img class="card-img-top" src="<?= base_url('assets/imagens/youtubedata.png') ?>" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title"><a>Guias</a></h4>
                    <p class="card-text">
                        Conheça a introdução da api, documentos destinados a desenvolvedores que desejam criar aplicativos
                        que interagem com o YouTube, explica os conceitos básicos do YouTube e da própria API. Também
                        oferece uma visão geral das diferentes funções compatíveis com a API...
                    </p>
                    <a href="https://developers.google.com/youtube/v3/getting-started" class="btn btn-danger">Acessar</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-4 mb-2">
            <div class="card">
                <img class="card-img-top" src="<?= base_url('assets/imagens/youtubedata2.png') ?>" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title"><a>API Reference</a></h4>
                    <p class="card-text">
                        A API de dados do YouTube permite a incorporação de funções normalmente executadas no site do YouTube em seu
                        próprio site ou aplicativo. Conheça a lista que identifica os diferentes tipos de recursos que você pode
                        recuperar usando a API...
                    </p>
                    <a href="https://developers.google.com/youtube/v3/docs/" class="btn btn-danger">Acessar</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-4 mb-2">
            <div class="card">
                <img class="card-img-top" src="<?= base_url('assets/imagens/youtubedata3.png') ?>" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title"><a>Exemplos de código</a></h4>
                    <p class="card-text">
                        Api disponibiliza algumas amostras de códigos em diferentes linguagem como: Go, Java, Javascript, .NET., Phyton,
                        PHP e Ruby. Conheça as listas que identificam os exemplos de código disponíveis para a API de dados do YouTube (v3)...
                    </p>
                    <a href="https://developers.google.com/youtube/v3/code_samples/" class="btn btn-danger">Acessar</a>
                </div>
            </div>
        </div>
    </div>
</div>