<div class="container mt-3 text-center">
    <div class="row">
        <div class="col-md-4 mt-4">
            <img src="<?= base_url('assets/imagens/youtubelogo.png') ?>" class="img-fluid mr-4" width="100%" alt="Youtube Data">
        </div>
        <div class="col-md-8">
            <h1 class="font-weight-bold">Youtube Data</h1>
            <div class="row mt-2">
                <div class="col-md-6 text-left mt-2">A API de dados do YouTube permite a incorporação de funções normalmente executadas
                    no site do YouTube em seu próprio site ou aplicativo. Com a API de dados do YouTube, você pode adicionar vários recursos
                    do YouTube ao seu aplicativo. API para enviar vídeos, gerenciar playlists e inscrições,
                    atualizar configurações de canal e muito mais.</div>
                <div class="col-md-6 text-left mt-2">Uma API é o conjunto de ferramentas que o software tem para se conectar a outro software,
                    ou seja, é um protocolo de comunicação entre diferentes programas ou, no caso do YouTube, entre diferentes
                    serviços da web. Uma API tem certas características e há coisas que ela permite e outras que não, se 
                    queremos obter dados de uma API, temos que saber como solicitar esses dados.</div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <h3>O que é possível criar com a Api?</h3><br />
                    <p>É poosível utilizar quase todos os recursos disponibilizados pelo site do Youtube, operações como,
                        recuperar, editar e remover vídeos, entre as utilizações mais comuns da api, são: uplaod e criação
                        de playlist.
                    </p>
                </div>
                <div class="col-md-6 text-left">
                    <h3>Quais são suas restrições?</h3><br />
                    <p>Para utilizar api é necessário a criação de uma chave de acesso através do google console para desenvolvedores.
                        E para acessar ou utilizar comandos, que sejam paryiculares, é necessário a criação de um Id Auth2.0 tambem no google console.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>