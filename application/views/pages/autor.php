<div class="container mt-3 text-center">
    <div class="row">
        <div class="col-md-8">
            <hr />
            <h1 class="font-weight-bold">Rafael Dionizio Santos</h1>
            <hr />
            <div class="row align-items-center">
                <div class="mt-4 col-md-6"><h3>RA: GU3001628</h3></div>
                <div class="mt-4 col-md-6"><h3>Tema: Youtube Data</h3></div>
                <div class="mt-4 col-md-6"><h3>Matéria: Linguagem de programação II</h3></div>
                <div class="mt-4 col-md-6"><h3>Curso: Análise e desenvolvimento de sistemas</h3></div>
            </div>
        </div>
        <div class="col-md-4">
            <img src="<?= base_url('assets/imagens/autor.jpg') ?>" class="img-fluid mr-4" width="400px" alt="Autor">
        </div>
    </div>
</div>