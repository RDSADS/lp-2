<nav class="mb-1 navbar navbar-expand-lg navbar-dark #59698d mdb-color lighten-1">
  <a class="navbar-brand" href="<?= base_url('home') ?>" >
  <img src="<?= base_url('assets/imagens/logo1.png') ?>" class="img-fluid mr-4" width="180px" alt="Logo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto font-weight-bold">
      <li class="nav-item active mt-1 mr-4">
        <a class="menu mt-1 mr-4" href="<?= base_url('at02/rafael') ?>" >
          Home
        </a>
      </li>
      <li class="nav-item mt-1 mr-5">
        <a class="menu" href="<?= base_url('at02/playlist') ?>">Playlist</a>
      </li>
      <li class="nav-item mt-1 mr-5">
        <a class="menu" href="<?= base_url('at02/relatorio') ?>">Relatório</a>
      </li>
      <li class="nav-item mt-1 mr-5">
        <a class="menu" href="<?= base_url('at02') ?>">Autor</a>
      </li>
      <li class="nav-item mt-1 mr-1">
        <a class="menu" href="<?= base_url('at02/api') ?>">API</a>
      </li>
    </ul>
    <img src="<?= base_url('assets/imagens/musicmenu.png') ?>" class="img-fluid mr-5" width="250px" alt="Imagem Menu">
  </div>
</nav>