        <script type="text/javascript" src="<?= base_url('assets/mdb/js/mdb.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/mdb/js/popper.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/mdb/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/mdb/js/mdb.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/JavaScript.js') ?>"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.3/js/mdb.min.js"></script>

    <footer class="page-footer font-small #59698d mdb-color lighten-1 mt-3 pt-4">

      <div class="container text-center text-md-left">
        <div class="row ">
          <div class="col-md-12 text-center ol-lg-12 mr-auto my-md-4 my-0 mt-4 mb-1">
            <h5 class="footertitle font-weight-bold text-center text-uppercase mb-4">ListMusic</h5>
            <p class="footer font-weight-bold">Monte sua playlist de clipes musicais, veja quando quiser e compartilhe com outros membros, 
              troque experiências.</p>
          </div>
        </div>
      </div>
      <div class="footer-copyright text-center py-3">© 2019:
        <a href="#"> Rafael Dionizio</a>
      </div>
    </footer>
  </body>
</html>